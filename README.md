![image](https://img-c.udemycdn.com/course/750x422/2777074_6d96_11.jpg)


# 🚀 Spring Finance Proxy

Bem-vindo(a). Este é o Spring Finance Proxy!

O objetivo desta aplicação é coordenar as rotas da palicação Spring Finance, com uma pegada de Api Gateway.

# Estrutura da app:
events {  }

http {
    server {
        listen 80;
        server_name spring-finances-web;

        location / {
            proxy_pass http://spring-finances-web:3000;
        }
    }

    server {
        listen 9091;
        server_name pgadmin-compose;

        location / {
            proxy_pass http://pgadmin-compose:80;
        }
    }

    server {
        listen 9092;
        server_name postgres;

        location / {
            proxy_pass http://postgres:5432;
        }
    }

    server {
        listen 9093;
        server_name spring-finances;

        location / {
            proxy_pass http://spring-finances:8080;
        }
    }

    server {
        listen 9094;
        server_name spring-finance-prometheus;

        location / {
            proxy_pass http://spring-finance-prometheus:9090;
        }
    }
}

# Utilização personalizada da aplicação:
- Para passar configuações diferentes da padrão via docker run ou docker compose, passe o comando -v e o caminho do arquivo a substituir.
- Exemplo: 	docker run --name spring-finance-proxy -v C:/GitEstudo/proxys/go-finance-proxy/config/nginx.conf:/etc/nginx/nginx.conf -p 80:80 -p 9090:9090 -p 9091:9091 -p 9092:9092 -d marcelosilva404/spring-finance-proxy:1.0

Made with 💜 at OliveiraDev